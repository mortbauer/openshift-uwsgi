uWSGI on OpenShift
==================

This git repository helps you running a uWSGI server quickly on OpenShift.
The default configuration runs the uWSGI server with a WSGI Python app but
you can run whatever you want as long as uWSGI supports it.

You can configure how to build the uWSGI sever in the *"uwsgi_build.ini"*
file if you don't want to run uWSGI with Python or if you want to install other
plugins.

There is also an _"uwsgi.yaml"_ configuration file that you can decide how to
run the uWSGI server. You can change anything there, including the path to
the static directory or the path to your _"wsgi.py"_ file (of course you don't need this file if you don't want to run Python with uWSGI).

Another good news for Python developers. Although the OpenShift comes with a
_python-2.6_ type template, but it lacks support for installing dependencies
through _pip_'s _requirements.txt_ file. It's supported in this template
out-of-box! For other developers, please feel free to delete the "requirements.txt" file at the repository's root directory.

Running on OpenShift
====================
Create an account at [http://openshift.redhat.com/](http://openshift.redhat.com/)

Create a _diy-0.1_ app

    rhc app create -a $APP_NAME -t diy-0.1 -l $YOUR_LOGIN_EMAIL

Add this upstream repo

    cd $APP_NAME
    git remote add upstream -m master https://bitbucket.org/ollix/openshift-uwsgi.git
    git pull -s recursive -X theirs upstream master

Then push the repo upstream

    git push

That's it, you can now checkout your application at

    http://$APP_NAME-$YOUR_NAMESPACE.rhcloud.com

Tutorial
========
 * [Runing a Django app on OpenShift through the openshift-uwsgi template](https://bitbucket.org/ollix/openshift-uwsgi/wiki/running_django_app)
